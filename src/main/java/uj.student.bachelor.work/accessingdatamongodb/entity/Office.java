package uj.student.bachelor.work.accessingdatamongodb.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Office {

    @Id
    private String _id;

    private int roomNumber;
    private int floor;

    public Office() {

    }

    public Office(int roomNumber, int floor) {
        this.roomNumber = roomNumber;
        this.floor = floor;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }
}
