package uj.student.bachelor.work.accessingdatamongodb.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
public class Visit {

    @Id
    private String _id;

    private String patient;
    private String specialist;
    private String office;

    private LocalDateTime localDateTime;

    public Visit() {

    }

    public Visit(String _id, String specialist, String office, LocalDateTime localDateTime, String patient) {
        this._id = _id;
        this.specialist = specialist;
        this.office = office;
        this.localDateTime = localDateTime;
        this.patient = patient;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
