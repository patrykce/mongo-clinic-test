package uj.student.bachelor.work.accessingdatamongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import uj.student.bachelor.work.accessingdatamongodb.entity.Visit;

public interface VisitRepository extends MongoRepository<Visit, String> {
}
