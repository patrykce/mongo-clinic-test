package uj.student.bachelor.work.accessingdatamongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import uj.student.bachelor.work.accessingdatamongodb.entity.Office;

public interface OfficeRepository extends MongoRepository<Office, String> {
}
