package uj.student.bachelor.work.accessingdatamongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import uj.student.bachelor.work.accessingdatamongodb.entity.Specialist;

public interface SpecialistRepository extends MongoRepository<Specialist, String> {
}
