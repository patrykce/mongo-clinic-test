package uj.student.bachelor.work.accessingdatamongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import uj.student.bachelor.work.accessingdatamongodb.entity.Patient;

public interface PatientRepository extends MongoRepository<Patient, String> {
}
