package uj.student.bachelor.work.accessingdatamongodb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uj.student.bachelor.work.accessingdatamongodb.dto.OfficeDTO;
import uj.student.bachelor.work.accessingdatamongodb.entity.Office;
import uj.student.bachelor.work.accessingdatamongodb.service.OfficeService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/office")
public class OfficeController {

    private final OfficeService officeService;

    @Autowired
    public OfficeController(OfficeService officeService) {
        this.officeService = officeService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Office> getById(@PathVariable String id) {
        Optional<Office> office = officeService.getById(id);
        return office.map(office_ -> new ResponseEntity<>(office_, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public ResponseEntity<List<Office>> getBy() {
        List<Office> offices = officeService.getAll();
        return new ResponseEntity<>(offices, HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<Office> save(@RequestBody @Valid OfficeDTO officeDTO) {
        Office office = officeService.save(officeDTO);
        return new ResponseEntity<>(office, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    ResponseEntity<Optional<Office>> update(@PathVariable String id, @RequestBody @Valid OfficeDTO officeDTO) {
        Optional<Office> office = officeService.update(id, officeDTO);
        return new ResponseEntity<>(office, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable String id) {
        officeService.deleteById(id);
    }
}
