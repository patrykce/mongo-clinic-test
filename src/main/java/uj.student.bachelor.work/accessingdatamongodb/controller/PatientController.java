package uj.student.bachelor.work.accessingdatamongodb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uj.student.bachelor.work.accessingdatamongodb.dto.PatientDTO;
import uj.student.bachelor.work.accessingdatamongodb.entity.Patient;
import uj.student.bachelor.work.accessingdatamongodb.service.PatientService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Patient> getById(@PathVariable String id) {
        Optional<Patient> patient = patientService.getById(id);
        return patient.map(patinet_ -> new ResponseEntity<>(patinet_, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public ResponseEntity<List<Patient>> getBy() {
        List<Patient> patients = patientService.getAll();
        return new ResponseEntity<>(patients, HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<Patient> save(@RequestBody @Valid PatientDTO patientDTO) {
        Patient patient = patientService.save(patientDTO);
        return new ResponseEntity<>(patient, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    ResponseEntity<Patient> update(@PathVariable String id, @RequestBody @Valid PatientDTO patientDTO) {
        Patient patient = patientService.update(id, patientDTO);
        return new ResponseEntity<>(patient, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable String id) {
        patientService.deleteById(id);
    }
}
