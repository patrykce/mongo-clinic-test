package uj.student.bachelor.work.accessingdatamongodb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uj.student.bachelor.work.accessingdatamongodb.dto.VisitDTO;
import uj.student.bachelor.work.accessingdatamongodb.dto.VisitDTOResult;
import uj.student.bachelor.work.accessingdatamongodb.entity.Visit;
import uj.student.bachelor.work.accessingdatamongodb.service.VisitService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/visit")
public class VisitController {

    private final VisitService visitService;

    @Autowired
    public VisitController(VisitService visitService) {
        this.visitService = visitService;

    }

    @GetMapping("/{id}")
    public ResponseEntity<VisitDTOResult> getById(@PathVariable String id) {
        Optional<VisitDTOResult> visit = visitService.getById(id);
        return visit.map(visit_ -> new ResponseEntity<>(visit_, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public ResponseEntity<List<VisitDTOResult>> getAll() {
        List<VisitDTOResult> visitDTOResultList = visitService.getAll();
        return new ResponseEntity<>(visitDTOResultList, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<VisitDTO> save(@RequestBody @Valid VisitDTO visitDTO) {
        boolean result = visitService.save(visitDTO);
        if (result) {
            return new ResponseEntity<>(visitDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(visitDTO, HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/{id}")
    ResponseEntity<Optional<Visit>> update(@PathVariable String id, @RequestBody @Valid VisitDTO visitDTO) {
        Optional<Visit> visit = visitService.update(id, visitDTO);
        return new ResponseEntity<>(visit, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        visitService.deleteById(id);
    }
}
