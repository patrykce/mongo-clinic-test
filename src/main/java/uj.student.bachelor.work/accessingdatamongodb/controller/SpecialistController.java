package uj.student.bachelor.work.accessingdatamongodb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uj.student.bachelor.work.accessingdatamongodb.dto.SpecialistDTO;
import uj.student.bachelor.work.accessingdatamongodb.dto.SpecialistDTOResult;
import uj.student.bachelor.work.accessingdatamongodb.entity.Specialist;
import uj.student.bachelor.work.accessingdatamongodb.service.SpecialistService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/specialist")
public class SpecialistController {

    private final SpecialistService specialistService;

    @Autowired
    public SpecialistController(SpecialistService specialistService) {
        this.specialistService = specialistService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<SpecialistDTOResult> getById(@PathVariable String id) {
        Optional<SpecialistDTOResult> specialist = specialistService.getById(id);
        return specialist.map(specialist_ -> new ResponseEntity<>(specialist_, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public ResponseEntity<List<SpecialistDTOResult>> getAll() {
        List<SpecialistDTOResult> specialists = specialistService.getAll();
        return new ResponseEntity<>(specialists, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<SpecialistDTO> save(
            @RequestBody @Valid SpecialistDTO specialist) {
        specialistService.save(specialist);
        return new ResponseEntity<>(specialist, HttpStatus.OK);
    }


    @PatchMapping("/bind/{id_1}/{id_2}")
    public ResponseEntity<String> bindOfficeWithSpecialist(@PathVariable String id_1, @PathVariable String id_2) {
        boolean bind = specialistService.bindOfficeWithSpecialist(id_1, id_2);
        if (bind) {
            return new ResponseEntity<>("bind office with id " + id_1 + " with specialist with id " + id_2, HttpStatus.OK);
        }
        return new ResponseEntity<>("cannot office with id " + id_1 + " with specialist with id " + id_2, HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/{id}")
    ResponseEntity<Specialist> update(@PathVariable String id, @RequestBody @Valid SpecialistDTO specialistDTO) {
        Specialist specialist = specialistService.update(id, specialistDTO);
        return new ResponseEntity<>(specialist, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable String id) {
        specialistService.deleteById(id);
    }

}
