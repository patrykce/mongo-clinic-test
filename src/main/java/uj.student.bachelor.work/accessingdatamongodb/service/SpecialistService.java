package uj.student.bachelor.work.accessingdatamongodb.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import uj.student.bachelor.work.accessingdatamongodb.dto.SpecialistDTO;
import uj.student.bachelor.work.accessingdatamongodb.dto.SpecialistDTOResult;
import uj.student.bachelor.work.accessingdatamongodb.entity.Office;
import uj.student.bachelor.work.accessingdatamongodb.entity.Specialist;
import uj.student.bachelor.work.accessingdatamongodb.repository.OfficeRepository;
import uj.student.bachelor.work.accessingdatamongodb.repository.SpecialistRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SpecialistService {
    private final SpecialistRepository specialistRepository;
    private final OfficeRepository officeRepository;
    private final ModelMapper modelMapper;

    public SpecialistService(SpecialistRepository specialistRepository, OfficeRepository officeRepository, ModelMapper modelMapper) {
        this.specialistRepository = specialistRepository;
        this.officeRepository = officeRepository;
        this.modelMapper = modelMapper;
    }

    public Optional<SpecialistDTOResult> getById(String id) {
        Optional<Specialist> specialistOptional = specialistRepository.findById(id);
        if (specialistOptional.isEmpty()) {
            return Optional.empty();
        }
        Specialist specialist = specialistOptional.get();
        SpecialistDTOResult specialistDTOResult = specialistToSpecialistDTOResult(specialist);
        return Optional.of(specialistDTOResult);
    }

    public List<SpecialistDTOResult> getAll() {
        List<Specialist> specialists = specialistRepository.findAll();
        List<SpecialistDTOResult> specialistDTOResultList = new ArrayList<>();
        for (Specialist specialist : specialists) {
            SpecialistDTOResult specialistDTOResult = specialistToSpecialistDTOResult(specialist);
            specialistDTOResultList.add(specialistDTOResult);
        }
        return specialistDTOResultList;
    }

    public Specialist update(String id, SpecialistDTO specialistDTO) {
        Specialist specialist = modelMapper.map(specialistDTO, Specialist.class);
        specialistRepository.save(specialist);
        return specialist;
    }

    public void save(SpecialistDTO specialistDTO) {
        Specialist specialist = modelMapper.map(specialistDTO, Specialist.class);
        specialist.setOffices(new ArrayList<>());
        List<String> ids = specialistDTO.getOfficesIDs();
        if (ids != null) {
            for (String id : ids) {
                Optional<Office> office = officeRepository.findById(id);
                office.ifPresent(value -> specialist.getOffices().add(id));
            }
        }
        specialistRepository.save(specialist);
    }

    public void deleteById(String id) {
        specialistRepository.deleteById(id);
    }

    private SpecialistDTOResult specialistToSpecialistDTOResult(Specialist specialist) {
        SpecialistDTOResult specialistDTOResult = new SpecialistDTOResult();
        List<String> officesAsString = specialist.getOffices();
        List<Office> offices = new ArrayList<>();
        for (String officeId : officesAsString) {
            Optional<Office> office = officeRepository.findById(officeId);
            office.ifPresent(offices::add);
        }
        specialistDTOResult.setOffices(offices);
        specialistDTOResult.set_id(specialist.get_id());
        specialistDTOResult.setFirstName(specialist.getFirstName());
        specialistDTOResult.setLastName(specialist.getLastName());
        specialistDTOResult.setPesel(specialist.getPesel());
        specialistDTOResult.setSpecialization(specialist.getSpecialization());
        return specialistDTOResult;
    }

    public boolean bindOfficeWithSpecialist(String id_1, String id_2) {
        Optional<Specialist> specialist_1 = specialistRepository.findById(id_1);
        Optional<Office> newOffice = officeRepository.findById(id_2);
        if (!(specialist_1.isPresent() && newOffice.isPresent())) {
            return false;
        }
        Specialist specialist = specialist_1.get();
        List<String> offices = specialist.getOffices() == null ? new ArrayList<>() : specialist.getOffices();
        offices.add(id_2);
        specialist_1.get().setOffices(offices);
        specialistRepository.save(specialist);
        return true;
    }

}
