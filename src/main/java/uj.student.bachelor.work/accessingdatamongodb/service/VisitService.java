package uj.student.bachelor.work.accessingdatamongodb.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import uj.student.bachelor.work.accessingdatamongodb.dto.SpecialistDTOResult;
import uj.student.bachelor.work.accessingdatamongodb.dto.VisitDTO;
import uj.student.bachelor.work.accessingdatamongodb.dto.VisitDTOResult;
import uj.student.bachelor.work.accessingdatamongodb.entity.Office;
import uj.student.bachelor.work.accessingdatamongodb.entity.Patient;
import uj.student.bachelor.work.accessingdatamongodb.entity.Visit;
import uj.student.bachelor.work.accessingdatamongodb.repository.OfficeRepository;
import uj.student.bachelor.work.accessingdatamongodb.repository.PatientRepository;
import uj.student.bachelor.work.accessingdatamongodb.repository.VisitRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VisitService {

    private final VisitRepository visitRepository;
    private final SpecialistService specialistService;
    private final OfficeRepository officeRepository;
    private final PatientRepository patientRepository;

    private final ModelMapper modelMapper;

    public VisitService(VisitRepository visitRepository, OfficeRepository officeRepository,
                        PatientRepository patientRepository, SpecialistService specialistService,
                        ModelMapper modelMapper) {
        this.visitRepository = visitRepository;
        this.specialistService = specialistService;
        this.officeRepository = officeRepository;
        this.patientRepository = patientRepository;
        this.modelMapper = modelMapper;
    }

    public Optional<VisitDTOResult> getById(String uuid) {
        Optional<Visit> visitOptional = visitRepository.findById(uuid);
        if (visitOptional.isEmpty()) {
            return Optional.empty();
        }
        Visit visit = visitOptional.get();
        VisitDTOResult visitDTOResult = visitToVisitDTOResult(visit);
        return Optional.of(visitDTOResult);
    }

    public List<VisitDTOResult> getAll() {
        List<Visit> visits = visitRepository.findAll();
        List<VisitDTOResult> visitDTOResultList = new ArrayList<>();
        for (Visit visit : visits) {
            VisitDTOResult visitDTOResult = visitToVisitDTOResult(visit);
            visitDTOResultList.add(visitDTOResult);
        }
        return visitDTOResultList;
    }

    public boolean save(VisitDTO visitDTO) {
        Visit visit = modelMapper.map(visitDTO, Visit.class);

        Optional<SpecialistDTOResult> specialist = specialistService.getById(visitDTO.getSpecialistID());
        Optional<Office> office = officeRepository.findById(visitDTO.getOfficeID());
        Optional<Patient> patient = patientRepository.findById(visitDTO.getPatientID());

        if (specialist.isEmpty() || office.isEmpty() || patient.isEmpty()) {
            return false;
        }

        visit.setPatient(visitDTO.getPatientID());
        visit.setOffice(visitDTO.getOfficeID());
        visit.setSpecialist(visitDTO.getSpecialistID());
        visitRepository.save(visit);
        return true;
    }

    public Optional<Visit> update(String id, VisitDTO visitDTO) {
        Visit visit = modelMapper.map(visitDTO, Visit.class);
        Optional<Visit> visitToUpdate = visitRepository.findById(id);
        if (visitToUpdate.isPresent()) {
            return Optional.empty();
        }
        visitRepository.save(visit);
        return Optional.of(visit);
    }

    public void deleteById(String id) {
        visitRepository.deleteById(id);
    }

    private VisitDTOResult visitToVisitDTOResult(Visit visit) {
        VisitDTOResult visitDTOResult = new VisitDTOResult();
        Optional<Patient> patientOptional = patientRepository.findById(visit.getPatient());
        Optional<Office> officeOptional = officeRepository.findById(visit.getOffice());
        Optional<SpecialistDTOResult> specialistOptional = specialistService.getById(visit.getSpecialist());
        patientOptional.ifPresent(visitDTOResult::setPatient);
        officeOptional.ifPresent(visitDTOResult::setOffice);
        specialistOptional.ifPresent(visitDTOResult::setSpecialistDTOResult);
        visitDTOResult.setLocalDateTime(visit.getLocalDateTime());
        visitDTOResult.set_id(visit.get_id());
        return visitDTOResult;
    }

}
