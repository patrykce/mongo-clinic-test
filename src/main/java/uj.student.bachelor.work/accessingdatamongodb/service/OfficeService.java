package uj.student.bachelor.work.accessingdatamongodb.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import uj.student.bachelor.work.accessingdatamongodb.dto.OfficeDTO;
import uj.student.bachelor.work.accessingdatamongodb.entity.Office;
import uj.student.bachelor.work.accessingdatamongodb.repository.OfficeRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OfficeService {
    private final OfficeRepository officeRepository;
    private final ModelMapper modelMapper;

    public OfficeService(OfficeRepository officeRepository, ModelMapper modelMapper) {
        this.officeRepository = officeRepository;
        this.modelMapper = modelMapper;
    }

    public Optional<Office> getById(String id) {
        return officeRepository.findById(id);
    }

    public List<Office> getAll() {
        return officeRepository.findAll();
    }

    public Office save(OfficeDTO officeDTO) {
        Office office = modelMapper.map(officeDTO, Office.class);
        officeRepository.save(office);
        return office;
    }

    public Optional<Office> update(String id, OfficeDTO officeDTO) {
        Optional<Office> officeToUpdate = officeRepository.findById(id);
        if (officeToUpdate.isPresent()) {
            Office office = modelMapper.map(officeDTO, Office.class);
            office.set_id(id);
            officeRepository.save(office);
            return Optional.of(office);
        }
        return Optional.empty();
    }

    public void deleteById(String id) {
        officeRepository.deleteById(id);
    }
}
