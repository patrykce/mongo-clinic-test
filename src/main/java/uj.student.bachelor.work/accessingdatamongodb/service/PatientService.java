package uj.student.bachelor.work.accessingdatamongodb.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import uj.student.bachelor.work.accessingdatamongodb.dto.PatientDTO;
import uj.student.bachelor.work.accessingdatamongodb.entity.Patient;
import uj.student.bachelor.work.accessingdatamongodb.repository.PatientRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PatientService {
    private final PatientRepository patientRepository;
    private final ModelMapper modelMapper;

    public PatientService(PatientRepository patientRepository, ModelMapper modelMapper) {
        this.patientRepository = patientRepository;
        this.modelMapper = modelMapper;
    }

    public Optional<Patient> getById(String id) {
        return patientRepository.findById(id);
    }

    public List<Patient> getAll() {
        return patientRepository.findAll();
    }

    public Patient save(PatientDTO patientDTO) {
        Patient patient = modelMapper.map(patientDTO, Patient.class);
        return patientRepository.save(patient);
    }

    public Patient update(String id, PatientDTO patientDTO) {
        Patient patient = modelMapper.map(patientDTO, Patient.class);
        patientRepository.save(patient);
        return patient;
    }

    public void deleteById(String id) {
        patientRepository.deleteById(id);
    }
}
