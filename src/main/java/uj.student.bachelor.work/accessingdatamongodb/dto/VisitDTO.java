package uj.student.bachelor.work.accessingdatamongodb.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

public class VisitDTO {

    private final LocalDateTime localDateTime;

    @NotEmpty
    private final String patientID;

    @NotEmpty
    private final String specialistID;

    @NotEmpty
    private final String officeID;

    @JsonCreator
    public VisitDTO(@JsonProperty("localDateTime") LocalDateTime localDateTime,
                    @JsonProperty("patientIDs") String patientID,
                    @JsonProperty("specialistIDs") String specialistID,
                    @JsonProperty("officeIDs") String officeID
    ) {
        this.localDateTime = localDateTime;
        this.patientID = patientID;
        this.specialistID = specialistID;
        this.officeID = officeID;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public String getPatientID() {
        return patientID;
    }

    public String getSpecialistID() {
        return specialistID;
    }

    public String getOfficeID() {
        return officeID;
    }
}
