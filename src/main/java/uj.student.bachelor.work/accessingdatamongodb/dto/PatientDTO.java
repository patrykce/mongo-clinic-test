package uj.student.bachelor.work.accessingdatamongodb.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import uj.student.bachelor.work.accessingdatamongodb.validator.Pesel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class PatientDTO {

    @NotBlank
    private final String firstName;
    @NotBlank
    private final String lastName;
    @Pesel
    private final String pesel;
    @NotNull
    private final LocalDate dateOfBirth;


    @JsonCreator
    public PatientDTO(
            @JsonProperty("firstName") String firstName,
            @JsonProperty("lastName") String lastName,
            @JsonProperty("pesel") String pesel,
            @JsonProperty("dayOfBirth") LocalDate birthday) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.dateOfBirth = birthday;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }
}
