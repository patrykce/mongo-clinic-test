package uj.student.bachelor.work.accessingdatamongodb.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class OfficeDTO {

    @NotNull
    private final int roomNumber;
    @NotNull
    private final int floor;

    @JsonCreator
    public OfficeDTO(
            @JsonProperty("roomNumber") int roomNumber,
            @JsonProperty("floor") int floor) {

        this.roomNumber = roomNumber;
        this.floor = floor;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public int getFloor() {
        return floor;
    }
}
