package uj.student.bachelor.work.accessingdatamongodb.dto;

import uj.student.bachelor.work.accessingdatamongodb.entity.Office;
import uj.student.bachelor.work.accessingdatamongodb.entity.Patient;

import java.time.LocalDateTime;

public class VisitDTOResult {

    private String _id;

    private Patient patient;

    private SpecialistDTOResult specialistDTOResult;

    private Office office;

    private LocalDateTime localDateTime;

    public VisitDTOResult() {

    }

    public VisitDTOResult(String _id, Patient patient, SpecialistDTOResult specialistDTOResult, Office office, LocalDateTime localDateTime) {
        this._id = _id;
        this.patient = patient;
        this.specialistDTOResult = specialistDTOResult;
        this.office = office;
        this.localDateTime = localDateTime;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public SpecialistDTOResult getSpecialistDTOResult() {
        return specialistDTOResult;
    }

    public void setSpecialistDTOResult(SpecialistDTOResult specialistDTOResult) {
        this.specialistDTOResult = specialistDTOResult;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
