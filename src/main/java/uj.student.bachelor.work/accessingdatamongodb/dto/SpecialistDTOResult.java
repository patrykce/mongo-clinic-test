package uj.student.bachelor.work.accessingdatamongodb.dto;

import uj.student.bachelor.work.accessingdatamongodb.entity.Office;

import java.util.List;

public class SpecialistDTOResult {

    private String _id;
    private String firstName;
    private String lastName;
    private String pesel;
    private String specialization;
    private List<Office> offices;

    public SpecialistDTOResult() {

    }

    public SpecialistDTOResult(String _id, String firstName, String lastName, String pesel, String specialization, List<Office> offices) {
        this._id = _id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.specialization = specialization;
        this.offices = offices;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public List<Office> getOffices() {
        return offices;
    }

    public void setOffices(List<Office> offices) {
        this.offices = offices;
    }
}
