package uj.student.bachelor.work.accessingdatamongodb.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import uj.student.bachelor.work.accessingdatamongodb.validator.Pesel;

import javax.validation.constraints.NotBlank;
import java.util.List;

public class SpecialistDTO {

    @NotBlank
    private final String firstName;
    @NotBlank
    private final String lastName;
    @Pesel
    private final String pesel;
    @NotBlank
    private final String specialization;

    private final List<String> officesIDs;

    @JsonCreator
    public SpecialistDTO(
            @JsonProperty("firstName") String firstName,
            @JsonProperty("lastName") String lastName,
            @JsonProperty("pesel") String pesel,
            @JsonProperty("specialization") String specialization,
            @JsonProperty("officesIDs") List<String> officesIDs) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.specialization = specialization;
        this.officesIDs = officesIDs;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public String getSpecialization() {
        return specialization;
    }

    public List<String> getOfficesIDs() {
        return officesIDs;
    }
}
