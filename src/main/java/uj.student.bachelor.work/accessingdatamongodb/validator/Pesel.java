package uj.student.bachelor.work.accessingdatamongodb.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PeselValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Pesel {
    String message() default "Invalid pesel number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

