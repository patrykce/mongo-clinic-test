package uj.student.bachelor.work.accessingdatamongodb.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PeselValidator implements
        ConstraintValidator<Pesel, String> {

    @Override
    public void initialize(Pesel pesel) {
    }

    @Override
    public boolean isValid(String contactField,
                           ConstraintValidatorContext cxt) {
        return (contactField.matches("[0-9]+")
                && (contactField.length() == 11));
    }

}
